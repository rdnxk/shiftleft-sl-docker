# ShiftLeft CLI

## What is this image
This image contains the [ShiftLeft CLI](https://docs.shiftleft.io/cli/reference/overview) (aka `sl`) for code security tools. ShiftLeft CLI includes interfaces for SAST, secret detection, insights, SCA, and security training.

For more information on ShiftLeft CLI, read their [documentation](https://docs.shiftleft.io/cli/reference/overview).

This image is automatically updated every week and is maintainted by `ReDemoNBR` and `rdnxk`. **_It is not maintained by ShiftLeft_**.

The motivation of this project instead of using official ShiftLeft images (ex: [shiftleft/scan](https://hub.docker.com/r/shiftleft/scan)) is that those images are built on top of [ubuntu:focal](https://hub.docker.com/_/ubuntu), which is heavier (\~800MB final size) compared to our Debian (\~330MB) or even Alpine (\~93MB) variants. Also, their images on Docker Hub are undocumented at the time of this writing

This image is designed to be used on CI/CD pipelines.

## Tags explanation
Format is `<version>-<variant>` or `<version>` (the latter one uses [buildpack-deps:bookworm-scm](https://hub.docker.com/_/buildpack-deps) as base image)

All images contain the `sl` executable ready for use. Also it is aliased to `shiftleft-cli` to avoid confusion with [sl (Steam Locomotive)](https://github.com/mtoyoda/sl).

### Image variants
#### **`redemonbr/shiftleft-cli:<version>`** | **`redemonbr/shiftleft-cli:<version>-bookworm`**
This is the defacto image, it is built on top of Debian's latest stable release ["bookworm"](https://www.debian.org/releases/bookworm/), through `buildpack-deps:bookworm-scm` available at [Buildpack official image](https://hub.docker.com/_/buildpack-deps).

#### **`redemonbr/shiftleft-cli:<version>-alpine`**
This image is built on top of the latest stable release of [Alpine Linux](https://alpinelinux.org), available at [Alpine official image](https://hub.docker.com/_/alpine).

This variant is useful when final image size being as small as possible is your primary concern. The main caveat to note is that it does use [`musl libc`](https://musl.libc.org) instead of [glibc and friends](https://www.etalabs.net/compare_libcs.html), so software will often run into issues depending on the depth of their libc requirements/assumptions.

## Source of these images
-   Repository: <https://gitlab.com/rdnxk/shiftleft-sl-docker>
-   Issues: <https://gitlab.com/rdnxk/shiftleft-sl-docker/-/issues>

## Usage
For ShiftLeft CLI usage with their options and arguments, please read their own [CLI documentation](https://docs.shiftleft.io/cli/reference/overview).
Here we will cover only the Docker image usage.

### Docker Run CLI
#### Basic `--help` usage
```bash
$ docker run redemonbr/shiftleft-cli:latest sl --help
```

#### Mounting application volume and analyzing it
```bash
$ docker run \
  --rm \
  --name shiftleft \
  --volume /path/to/my/application/:/mnt/my-app/ \
  --env SHIFTLEFT_APP=my-app \
  --env SHIFTLEFT_CPG=true \
  --env SHIFTLEFT_NO_DIAGNOSTIC=true \
  --env SHIFTLEFT_ACCESS_TOKEN=<access_token> \
  --env SHIFTLEFT_LANG_JAVA=true \
  --env SHIFTLEFT_JAVA_OPTS=-Xmx10g \
  redemonbr/shiftleft-cli:bookworm \
  /bin/bash

root@942b0046a022:/# cd /mnt/my-app
root@942b0046a022:/# sl auth
root@942b0046a022:/# sl analyze my-app.jar --dep dependency-1.jar --dep dependency-2.jar
```
#### Mounting application volume and using multiple commands
```console
$ docker run \
  -it \
  --rm \
  --name shiftleft \
  --volume /path/to/my/application/:mnt/my-app/ \
  --env SHIFTLEFT_APP=my-app \
  --env SHIFTLEFT_CPG=true \
  --env SHIFTLEFT_NO_DIAGNOSTIC=true \
  --env SHIFTLEFT_ACCESS_TOKEN=<access_token> \
  --env SHIFTLEFT_LANG_JAVA=true \
  --env SHIFTLEFT_JAVA_OPTS=-Xmx10g \
  redemonbr/shiftleft-cli:alpine \
  /bin/sh

/ # cd /mnt/my-app
/ # shiftleft-cli auth
/ # shiftleft-cli analyze my-app.jar --dep dependency-1.jar --dep dependency-2.jar --tag branch=my-tag
/ # shiftleft-cli check-analysis --branch "my-tag" --report --report-file "./shiftleft.md"
```

### GitLab-CI (Continuous Integration example)
Using on GitLab-CI for a NodeJS project
```yml
audit:sast-scan:
  stage: test
  image: redemonbr/shiftleft-cli:alpine
  variables:
    SHIFTLEFT_APP: my-great-app
    SHIFTLEFT_CPG: true
    SHIFTLEFT_LANG_JS: true
    SHIFTLEFT_NO_DIAGNOSTIC: true
    SHIFTLEFT_WAIT: true
    ## replace the ones below with yours, or (better) put them into your CI/CD secrets
    SHIFTLEFT_ACCESS_TOKEN: <token_here>
    SHIFTLEFT_ORG: <org_id_here>
  before_script:
    - npm install
    - shiftleft-cli auth --org "$SHIFTLEFT_ORG"
  script:
    - shiftleft-cli analyze --tag "branch=$CI_COMMIT_REF_NAME" ./ -- --no-babel --exclude "node_modules/,test/,.git/,.cache/"
    # create directory to store report
    - mkdir .reports/
    - shiftleft-cli check-analysis --branch "$CI_COMMIT_REF_NAME" --report --report-file "./.reports/shiftleft.md"
  after_script:
    # copy report to another directory
    - cp .reports/shiftleft.md ./shiftleft.md
  artifacts:
    name: shiftleft-$CI_COMMIT_SHORT_SHA
    paths:
      - shiftleft.md
    expire_in: 7 days
    public: false
```
