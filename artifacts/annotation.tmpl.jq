{
    ($input_file): {
        "org.opencontainers.image.title": $output_file,
        "org.opencontainers.image.created": $date,
        "org.opencontainers.image.description": $file_description
    },
    "$config": {
        "org.opencontainers.image.created": $date
    },
    "$manifest": {
        "org.opencontainers.image.title": "ShiftLeft SL",
        "org.opencontainers.image.description": "A code security platform for developers",
        "org.opencontainers.image.vendor": "rdnxk",
        "org.opencontainers.image.authors": "rdnxk, San 'rdn' Mônico <san@monico.com.br>",
        "org.opencontainers.image.url": "https://hub.docker.com/r/redemonbr/shiftleft-sl-docker",
        "org.opencontainers.image.source": "https://gitlab.com/rdnxk/shiftleft-sl-docker",
        "org.opencontainers.image.documentation": "https://gitlab.com/rdnxk/shiftleft-sl-docker/-/blob/README.md",
        "org.opencontainers.image.created": $date
    }
}
