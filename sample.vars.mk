# REQUIRED VARIABLES
BUILDER ?= podman
BUILD_PLATFORM ?= linux/amd64
LOCAL_IMAGE_NAME ?= local:temp
TAG_SUFFIX ?= bookworm
DISTRO ?= debian
REMOTE_IMAGE_REPO ?= localhost:5000/rdnxk/shiftleft-sl/test
COSIGN_PRIVATE_KEY ?= cosign.key
COSIGN_PUBLIC_KEY ?= cosign.pub
COSIGN_PASSWORD ?= foobar

# OPTIONAL VARIABLES
INSECURE_REGISTRY ?= false
BUILD_OPTS ?= --squash --jobs 2
PUSH_OPTS ?=

## THE "__" PREFIXED ARE NOT **DIRECTLY** USED IN THE MAKEFILE
__INNER_CAPABILITIES ?= SYS_ADMIN,SYS_CHROOT,MKNOD,NET_ADMIN,NET_RAW
__INNER_SECURITY_OPTS ?= --security-opt apparmor=unconfined --security-opt seccomp=unconfined --security-opt label=disable --security-opt unmask=/sys/fs/cgroup:/proc/sys
INNER_CONTAINER_RUN_OPTS ?= --cap-add ${__INNER_CAPABILITIES} ${__INNER_SECURITY_OPTS} --device /dev/fuse --cgroups=disabled
