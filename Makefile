all: build push sign sbom artifacts
default: all

-include vars.mk
include constants.mk

ifdef $(CI_PIPELINE_CREATED_AT)
ISO_DATE = $(CI_PIPELINE_CREATED_AT)
else
ISO_DATE = $(shell date -u +%Y-%m-%dT%H:%M:%SZ)
endif
URLSAFE_DATE :::= $(subst :,-,$(ISO_DATE))

ifneq ($(filter $(shell echo $(INSECURE_REGISTRY) | tr '[:upper:]' '[:lower:]'),true 1),)
BUILD_TLS_OPTS :::= --tls-verify=false
PUSH_TLS_OPTS :::= --tls-verify=false
SIGN_TLS_OPTS :::= --allow-insecure-registry
DIGEST_TLS_OPTS :::= --tls-verify=false
ARTIFACT_TLS_OPTS :::= --plain-http --insecure
else
BUILD_TLS_OPTS :::= --tls-verify
PUSH_TLS_OPTS :::= --tls-verify
SIGN_TLS_OPTS :::=
DIGEST_TLS_OPTS :::= --tls-verify
ARTIFACT_TLS_OPTS :::=
endif

REMOTE_IMAGE_NAME ?= $(REMOTE_IMAGE_REPO):$(TAG_SUFFIX)

export SUBMAKE = $(MAKE) --no-print-directory
export COSIGN_PASSWORD
export COSIGN_PRIVATE_KEY
export COSIGN_PUBLIC_KEY
export TEMP_DIR

get_platform_digest = $(shell \
	DIGEST_TLS_OPTS="$(DIGEST_TLS_OPTS)" \
    bash utils/get-digest.sh "$(REMOTE_IMAGE_NAME)" "$(1)" \
)
get_list_digest = $(shell \
	DIGEST_TLS_OPTS="$(DIGEST_TLS_OPTS)" \
	bash utils/get-digest.sh "$(REMOTE_IMAGE_NAME)" \
)
pretty_platform = $(subst /,,$(patsubst linux/%,%,$(1)))
get_version = $(shell \
	${INNER_CONTAINER_ENV} \
    ${BUILDER} run \
		--rm \
		${INNER_CONTAINER_RUN_OPTS} \
		--entrypoint 'sh' \
		$(LOCAL_IMAGE_NAME) -c \
        'cat /opt/shiftleft/manifest.json' | jq -r '.version' \
)

## TEMP DIR
$(TEMP_DIR):
	@mkdir -p $@

## CLEAN
clean:
	@rm -fr "${TEMP_DIR}"

## ARTIFACTS
.PHONY: artifacts
artifacts: $(TEMP_DIR)/license.json $(TEMP_DIR)/publickey.json $(TEMP_DIR)/readme.json

$(TEMP_DIR)/license.json: $(TEMP_DIR)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg input_file LICENSE.md \
		--arg output_file LICENSE.md \
		--arg file_description "${LICENSE_DESCRIPTION}" \
		--arg date "$(ISO_DATE)" > $@
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $@ \
		$(REMOTE_IMAGE_REPO):license,license_$(URLSAFE_DATE) \
		LICENSE.md:${MARKDOWN_TYPE}

$(TEMP_DIR)/publickey.json: $(TEMP_DIR)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg input_file cosign.pub \
		--arg output_file cosign.pub \
		--arg file_description "${PUBLICKEY_DESCRIPTION}" \
		--arg date "$(ISO_DATE)" > $@
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $@ \
		$(REMOTE_IMAGE_REPO):publickey,publickey_$(URLSAFE_DATE) \
		cosign.pub:${PEM_TYPE}

$(TEMP_DIR)/readme.json: $(TEMP_DIR)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg input_file README.md \
		--arg output_file README.md \
		--arg file_description "${README_DESCRIPTION}" \
		--arg date "$(ISO_DATE)" > $@
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $@ \
		$(REMOTE_IMAGE_REPO):readme,readme_$(URLSAFE_DATE) \
		README.md:${MARKDOWN_TYPE}


.PHONY: build
build:
	@echo Building for ${DISTRO} variant for ${BUILD_PLATFORM}
	@${BUILDER} build ${BUILD_OPTS} \
        $(BUILD_TLS_OPTS) \
        --manifest ${LOCAL_IMAGE_NAME} \
        --platform ${BUILD_PLATFORM} \
        --file container/${DISTRO}/Containerfile container/${DISTRO}/

.PHONY: push
push:
	@echo Pushing ${DISTRO} image
	$(eval version = $(call get_version))
	$(eval versions = $(shell bash utils/expand-versions.sh $(version) $(URLSAFE_DATE) $(TAG_SUFFIX)))
ifeq ($(TAG_SUFFIX), $(TAG_DEFAULT))
	$(eval versions += $(shell bash utils/expand-versions.sh $(version) $(URLSAFE_DATE)))
endif
	@$(foreach tag, $(versions), \
		echo Pushing $(REMOTE_IMAGE_REPO):$(tag) ; \
        $(BUILDER) manifest push ${PUSH_OPTS} $(PUSH_TLS_OPTS) --all $(LOCAL_IMAGE_NAME) docker://$(REMOTE_IMAGE_REPO):$(tag) ; \
    )

.PHONY: sbom
sbom:
	@echo SBOM and attestation
	$(eval platforms = $(subst $(comma), , $(BUILD_PLATFORM)))
	@$(foreach platform, $(platforms),\
		$(eval arch = $(call pretty_platform,$(platform))) \
		$(eval digest = $(call get_platform_digest,$(platform))) \
		TEMP_DIR=$(TEMP_DIR) arch=$(arch) digest=$(digest) \
			$(SUBMAKE) $(TEMP_DIR)/sbom-$(arch).syft.json ; \
	)

$(TEMP_DIR)/sbom-$(arch).syft.json: $(TEMP_DIR)
	@syft scan \
		--output syft-json=$@ \
		--output cyclonedx-json=$(subst syft,cyclonedx,$@) \
		$(REMOTE_IMAGE_REPO)@$(digest)
	@cp $@ ./$(notdir $@)
	@jq -nf artifacts/annotation.tmpl.jq \
		--arg input_file $(notdir $@) \
		--arg output_file $(notdir $@) \
		--arg file_description "${SBOM_DESCRIPTION}" \
		--arg date "$(ISO_DATE)" > $(TEMP_DIR)/sbom.json
	@oras push \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		--config artifacts/config.json:${CONFIG_TYPE} \
		--annotation-file $(TEMP_DIR)/sbom.json \
		$(REMOTE_IMAGE_REPO):$(subst :,-,$(digest)).sbom \
		$(notdir $@):${SYFT_JSON_TYPE}
	@rm ./$(notdir $@)
	@cosign attest --yes \
		--type cyclonedx \
		--predicate $(subst syft,cyclonedx,$@) \
		--key ${COSIGN_PRIVATE_KEY} $(REMOTE_IMAGE_REPO)@$(digest)
	@cosign verify-attestation \
		--type cyclonedx \
		--key ${COSIGN_PUBLIC_KEY} \
		$(REMOTE_IMAGE_REPO)@$(digest) > /dev/null

.PHONY: sign
sign:
	@echo Signing image and manifest list for ${DISTRO}
	$(eval digest = $(call get_list_digest))
	@cosign sign $(SIGN_TLS_OPTS) --yes --recursive --key ${COSIGN_PRIVATE_KEY} $(REMOTE_IMAGE_REPO)@$(digest)
	@cosign verify $(SIGN_TLS_OPTS) --key ${COSIGN_PUBLIC_KEY} $(REMOTE_IMAGE_REPO)@$(digest) > /dev/null

.PHONY: audit
audit:
	$(eval arch = $(call pretty_platform,$(PLATFORM)))
	$(eval digest = $(call get_platform_digest,$(PLATFORM)))
	@digest="$(digest)" file="sbom-$(arch).syft.json" $(SUBMAKE) $(TEMP_DIR)/grype-$(arch).txt
	@syft convert $(TEMP_DIR)/remote-sbom-$(arch).json -o syft-table=$(TEMP_DIR)/remote-sbom-$(arch).txt

.PRECIOUS: $(TEMP_DIR)/remote-sbom-%.json
$(TEMP_DIR)/remote-sbom-%.json: $(TEMP_DIR)
	$(eval arch = $(subst remote-sbom-,,$(notdir $(basename $@))))
	@echo Downloading SBOM for $(arch)
	@oras pull \
		${ARTIFACT_TLS_OPTS} \
		${ARTIFACT_OPTS} \
		$(REMOTE_IMAGE_REPO):$(subst :,-,$(digest)).sbom
	@mv $(file) $@

$(TEMP_DIR)/grype-%.txt: $(TEMP_DIR)/remote-sbom-%.json
	$(eval arch = $(subst grype-,,$(notdir $(basename $@))))
	@echo Checking vulnerabilities in $(arch)
	@grype --file $@ sbom:$<
