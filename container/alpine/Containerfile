FROM alpine:latest

ARG TARGETPLATFORM TARGETOS TARGETARCH TARGETVARIANT

LABEL \
    org.opencontainers.image.authors="rdnxk (https://rdnxk.com), San 'rdn' Mônico <san@rdnxk.com> (https://rdnxk.com)" \
    org.opencontainers.image.description="ShiftLeft CLI for SAST, secret detection, insights, SCA, and security training" \
    org.opencontainers.image.documentation="https://gitlab.com/rdnxk/shiftleft-sl-docker/-/blob/main/README.md" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.source="https://gitlab.com/rdnxk/shiftleft-sl-docker" \
    org.opencontainers.image.title="shiftleft-sl-docker" \
    org.opencontainers.image.url="https://gitlab.com/rdnxk/shiftleft-sl-docker" \
    org.opencontainers.image.vendor="rdnxk" \
    platform="$TARGETPLATFORM" \
    os="$TARGETOS" \
    arch="$TARGETARCH" \
    variant="$TARGETVARIANT"

RUN apk upgrade --update --no-cache && \
    apk add --no-cache --virtual .build-deps curl jq && \
    apk add --no-cache openjdk17-jre && \
    mkdir -p /opt/shiftleft/ && \
    curl -Lo /opt/shiftleft/manifest.json https://www.shiftleft.io/download/slmanifest-linux-x64.json && \
    curl -Lo /tmp/shiftleft.tar.gz "$(jq -r '.downloadURL' /opt/shiftleft/manifest.json)" && \
    echo "$(jq -r '.sha256' /opt/shiftleft/manifest.json)  /tmp/shiftleft.tar.gz" > /tmp/checksum.txt && \
    sha256sum -c /tmp/checksum.txt && \
    tar -xzf /tmp/shiftleft.tar.gz -C /usr/bin/ && \
    chmod +x /usr/bin/sl && \
    ln -s ./sl /usr/bin/shiftleft-cli && \
    apk del .build-deps && \
    rm /tmp/shiftleft.tar.gz && \
    sl --version

CMD ["sl", "--help"]
